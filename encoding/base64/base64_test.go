// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package base64_test

import (
	"bytes"
	"testing"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/encoding/base64"
)

func TestToBytes(t *testing.T) {
	tests := []struct {
		name          string
		base64String  base64.Base64String
		expectedBytes []byte
		expectError   bool
	}{
		{
			name:          "Valid base64 string",
			base64String:  base64.Base64String("SGVsbG8sIFdvcmxkIQ"),
			expectedBytes: []byte("Hello, World!"),
			expectError:   false,
		},
		{
			name:          "Invalid base64 string",
			base64String:  base64.Base64String("%"),
			expectedBytes: nil,
			expectError:   true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			messageBytes, err := tt.base64String.ToBytes()
			if (err != nil) != tt.expectError {
				t.Errorf("ToBytes() error = %v, expectError %v", err, tt.expectError)
				return
			}
			if !bytes.Equal(messageBytes, tt.expectedBytes) {
				t.Errorf("ToBytes() = %v, expected %v", messageBytes, tt.expectedBytes)
			}
		})
	}
}

func TestParseFromBytes(t *testing.T) {
	tests := []struct {
		name           string
		inputBytes     []byte
		expectedBase64 base64.Base64String
	}{
		{
			name:           "Valid bytes to base64",
			inputBytes:     []byte("Hello, World!"),
			expectedBase64: base64.Base64String("SGVsbG8sIFdvcmxkIQ"),
		},
		{
			name:           "Empty byte slice",
			inputBytes:     []byte(""),
			expectedBase64: base64.Base64String(""),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			base64String := base64.ParseFromBytes(tt.inputBytes)
			if base64String != tt.expectedBase64 {
				t.Errorf("ParseFromBytes() = %v, expected %v", base64String, tt.expectedBase64)
			}
		})
	}
}
