// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package base64

import "encoding/base64"

type Base64String string

func (b Base64String) ToBytes() ([]byte, error) {
	return base64.RawURLEncoding.DecodeString(string(b))
}

func ParseFromBytes(data []byte) Base64String {
	return Base64String(base64.RawURLEncoding.EncodeToString(data))
}
