package go_connect_test

import (
	"bytes"
	"crypto/elliptic"
	"strings"
	"testing"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/jwt/jwe"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/jwt/jws"

	"github.com/stretchr/testify/assert"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/aes"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ecdsa"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ecies"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/encoding/base64"
)

func TestECCrypto(t *testing.T) {
	//sh: openssl ecparam -name prime256v1 -genkey -out ec_private.pem
	privateKeyPemBytes := []byte(`-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIGgxRqP6AS9wTvOSwIGaNPgSvoTd76i42TgfxlOpZPa/oAoGCCqGSM49
AwEHoUQDQgAEs8O8gM1Fy9HSKM+LTVC6jmYopNpAwADBEhhL/NA9MCfPZVCs6+xt
H3kNqna9MVcxWgSvvn4R6plR2gkSl+HTMA==
-----END EC PRIVATE KEY-----
`)

	//sh: openssl ec -in ec_private.pem -pubout -out ec_public.pem
	publicKeyPemBytes := []byte(`-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEs8O8gM1Fy9HSKM+LTVC6jmYopNpA
wADBEhhL/NA9MCfPZVCs6+xtH3kNqna9MVcxWgSvvn4R6plR2gkSl+HTMA==
-----END PUBLIC KEY-----
`)

	t.Run("ECDSA - sign and verify with self generated key", func(t *testing.T) {
		privateKey, err := ec.GenerateKeyPair(elliptic.P256())
		assert.NoError(t, err)

		messageBytes := []byte("A short message.")

		signature, err := ecdsa.Sign(privateKey, messageBytes)
		assert.NoError(t, err)
		assert.NotNil(t, signature)

		valid := ecdsa.Verify(&privateKey.PublicKey, messageBytes, signature)

		assert.True(t, valid)
	})

	t.Run("ECDSA - sign and verify with pregenerated key", func(t *testing.T) {
		messageBytes := []byte("Another short message.")

		privateKey, err := ec.ParsePrivateKeyFromPem(privateKeyPemBytes)
		assert.NoError(t, err)

		signature, err := ecdsa.Sign(privateKey, messageBytes)
		assert.NoError(t, err)
		assert.NotNil(t, signature)

		publicKey, err := ec.ParsePublicKeyFromPem(publicKeyPemBytes)
		assert.NoError(t, err)

		valid := ecdsa.Verify(publicKey, messageBytes, signature)

		assert.True(t, valid)
	})

	t.Run("ECIES - encrypt and decrypt with self generated key", func(t *testing.T) {
		privateKey, err := ec.GenerateKeyPair(elliptic.P256())
		assert.NoError(t, err)

		plainText := "The messsage to encrypt and decrypt."

		nonce, ciphertext, authenticationTag, err := ecies.Encrypt(&privateKey.PublicKey, []byte(plainText))
		assert.NoError(t, err)
		assert.NotNil(t, nonce)
		assert.NotNil(t, ciphertext)
		assert.NotNil(t, authenticationTag)

		decryptedText, err := ecies.Decrypt(privateKey, nonce, ciphertext, authenticationTag)
		assert.NoError(t, err)

		assert.Equal(t, plainText, string(decryptedText))
	})

	t.Run("ECIES - encrypt and decrypt with self generated key encoded and decoded pem", func(t *testing.T) {
		privateKey, err := ec.GenerateKeyPair(elliptic.P256())
		assert.NoError(t, err)

		plainText := "The messsage to encrypt and decrypt."

		publicKeyPem, err := privateKey.PublicKey.ToPem()
		assert.NoError(t, err)

		ecPublicKey, err := ec.ParsePublicKeyFromPem(publicKeyPem)
		assert.NoError(t, err)

		nonce, ciphertext, authenticationTag, err := ecies.Encrypt(ecPublicKey, []byte(plainText))
		assert.NoError(t, err)
		assert.NotNil(t, ciphertext)

		privateKeyPem, err := privateKey.ToPem()
		assert.NoError(t, err)

		ecPrivateKey, err := ec.ParsePrivateKeyFromPem(privateKeyPem)
		assert.NoError(t, err)

		decryptedText, err := ecies.Decrypt(ecPrivateKey, nonce, ciphertext, authenticationTag)
		assert.NoError(t, err)

		assert.Equal(t, plainText, string(decryptedText))
	})

	t.Run("ECIES - encrypt and decrypt with pre generated key encoded and decoded pem", func(t *testing.T) {
		plainText := `{"document":{"type":{"name":"FINANCIAL_CLAIMS_INFORMATION_REQUEST"}},"documentSignature":"MEYCIQCl3PV/DZZ2YrbRQv1n2KIelgE1+Vl6s7+c2jhmbYJGDwIhAMM2EqC1XESgty47/7jJl3RU8zLENgivu4YovQcJB6+/","certificate":"ZXlKaGJHY2lPaUpGVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmhjSEJmYldGdVlXZGxjbDl2YVc0aU9pSXhNak0wTlNJc0ltRndjRjl0WVc1aFoyVnlYM0IxWW14cFkxOXJaWGtpT2lJdExTMHRMVUpGUjBsT0lGQlZRa3hKUXlCTFJWa3RMUzB0TFZ4dVRVWnJkMFYzV1VoTGIxcEplbW93UTBGUldVbExiMXBKZW1vd1JFRlJZMFJSWjBGRlprUjNjWE5tVDBocVFXMXhXRVZyU0U1cU9EZEhUemR5Y2s1Mk9GeHVPVVpoT0c5cmJqRkJObFZSYWs1R056UmljblpNVWs1NlRYRjZRbVJxVVd4UUwxaGFOMUZTV2t0VGJuRnVMemQzZWxseFUyRktlbkppZHowOVhHNHRMUzB0TFVWT1JDQlFWVUpNU1VNZ1MwVlpMUzB0TFMxY2JpSXNJbUZ3Y0Y5d2RXSnNhV05mYTJWNUlqb2lMUzB0TFMxQ1JVZEpUaUJRVlVKTVNVTWdTMFZaTFMwdExTMWNiazFHYTNkRmQxbElTMjlhU1hwcU1FTkJVVmxKUzI5YVNYcHFNRVJCVVdORVVXZEJSVXRLVmtveVlUVm5PQ3N4ZEhJd1VFOXpUR0ZzUVRGSmQybFNlRWRjYmpOck9WbGxjSEpCTjFSaFRVRmhMMkpMZEV0bGRtRm1TWGgyVEcxSlduWXdPVFJNVWtaeVR6RkdVMFZGVVZsTVNXMWlkV3AzTDBoclptYzlQVnh1TFMwdExTMUZUa1FnVUZWQ1RFbERJRXRGV1MwdExTMHRYRzRpTENKaWFYSjBhR1JoZEdVaU9pSXhPVGN3TFRBeExUQXhJaXdpWW5OdUlqb2lNVEl6TkRVMk56ZzVJaXdpWlhod0lqb3hOekUxTVRjNE1UTTBMQ0ptWVcxcGJIbGZibUZ0WlNJNklrUnZaU0lzSW1kcGRtVnVYMjVoYldVaU9pSktiMmh1SWl3aWFXRjBJam94TnpFMU1UYzNPRE0wTENKdVltWWlPakUzTVRVeE56YzNNVFI5LmI3bjF4Zy1Qd3piZ2xVelhHejQ0ZjJyWHl1Q0RqYTc5cHduWDBsMmxpU2lzWEFNNzBNOVJGNjlFbzJaMlpGR0dfWWN2OUEzYWd4UFRzTUFvek0xb2Vn","certificateType":"AppManagerJWTCertificate"}`

		ecPublicKey, err := ec.ParsePublicKeyFromPem([]byte(`-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEI2iRDd2/Dh6buDj4npvQU7XneR1t
CBZxdO2E1iwIg8i0HGLmnwwv0GvbfvjcxVJ8pt/aBETFwUDYJR8josG7ug==
-----END PUBLIC KEY-----
`))
		assert.NoError(t, err)

		nonce, ciphertext, authenticationTag, err := ecies.Encrypt(ecPublicKey, []byte(plainText))
		assert.NoError(t, err)
		assert.NotNil(t, ciphertext)

		ecPrivateKey, err := ec.ParsePrivateKeyFromPem([]byte(`-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIEmdgTOyaqJ7m/aC3IVSqDuhWybiEh2d43gprT1xW7k7oAoGCCqGSM49
AwEHoUQDQgAEI2iRDd2/Dh6buDj4npvQU7XneR1tCBZxdO2E1iwIg8i0HGLmnwwv
0GvbfvjcxVJ8pt/aBETFwUDYJR8josG7ug==
-----END EC PRIVATE KEY-----
`))
		assert.NoError(t, err)

		decryptedText, err := ecies.Decrypt(ecPrivateKey, nonce, ciphertext, authenticationTag)
		assert.NoError(t, err)

		assert.Equal(t, plainText, string(decryptedText))
	})

	t.Run("ECIES - encrypt and decrypt with pregenerated key", func(t *testing.T) {
		publicKey, err := ec.ParsePublicKeyFromPem(publicKeyPemBytes)
		assert.NoError(t, err)

		plainText := "Another messsage to encrypt and decrypt."

		nonce, ciphertext, authenticationTag, err := ecies.Encrypt(publicKey, []byte(plainText))
		assert.NoError(t, err)
		assert.NotNil(t, ciphertext)

		privateKey, err := ec.ParsePrivateKeyFromPem(privateKeyPemBytes)
		assert.NoError(t, err)

		decryptedText, err := ecies.Decrypt(privateKey, nonce, ciphertext, authenticationTag)
		assert.NoError(t, err)

		assert.Equal(t, plainText, string(decryptedText))
	})

	t.Run("ECIES - decrypt a ciphertext that was encrypted in Dart", func(t *testing.T) {
		privateKey, err := ec.ParsePrivateKeyFromPem(privateKeyPemBytes)
		assert.NoError(t, err)

		combinedCiphertext := base64.Base64String("BL7TRACj_RlzTvfOVXhmJCTk1LafPUEwcDvZGwhA4TBFbBvyT0rzYXuUGeagQ-71WKc0zcsfOxTly4YHdVkhptEF9pxRX2Ea729eBjDXgCWddh_8hEvYMp6fSSagu4n-_sNwpFArBsXlgl7ko1KAvuXYRdJ_-g")
		ciphertextBytes, err := combinedCiphertext.ToBytes()
		assert.NoError(t, err)
		nonceLength := 16
		authenticationTagLength := 16
		nonce := ciphertextBytes[:nonceLength]
		authenticationTag := ciphertextBytes[len(ciphertextBytes)-authenticationTagLength:]
		ciphertext := ciphertextBytes[nonceLength : len(ciphertextBytes)-authenticationTagLength]

		plaintext, err := ecies.Decrypt(privateKey, nonce, ciphertext, authenticationTag)
		assert.NoError(t, err)

		assert.Equal(t, "hello", string(plaintext))
	})

	t.Run("ECDSA - verify a message signed in Dart", func(t *testing.T) {
		publicKey, err := ec.ParsePublicKeyFromPem(publicKeyPemBytes)
		assert.NoError(t, err)

		signature := base64.Base64String("MEUCIFUOVqV8d2eTo0a3HqPdgnN7ycRyuMp3kz49-VT1oneJAiEA-e5xLAGwrY9ylBcsCvor-KEsrvfNy76kI34ppIn0ZT4")
		signatureBytes, err := signature.ToBytes()
		assert.NoError(t, err)

		result := ecdsa.Verify(publicKey, []byte("hello"), signatureBytes)
		assert.True(t, result)
	})

	t.Run("AES - encrypt and decrypt with self generated key", func(t *testing.T) {
		key, err := aes.GenerateKey(aes.AES128)
		assert.NoError(t, err)

		plaintext := []byte("This is a test message.")

		nonce, ciphertext, verificationTag, err := aes.Encrypt(key, plaintext)
		if err != nil {
			t.Fatalf("Failed to encrypt plaintext: %v", err)
		}

		decryptedText, err := aes.Decrypt(key, nonce, ciphertext, verificationTag)
		if err != nil {
			t.Fatalf("Failed to decrypt ciphertext: %v", err)
		}

		if !bytes.Equal(plaintext, decryptedText) {
			t.Fatalf("Decrypted text does not match plaintext. Got %s, expected %s", decryptedText, plaintext)
		}
	})

	t.Run("AES - encrypt and decrypt with self generated key with encoding", func(t *testing.T) {
		tests := []struct {
			keySize aes.AESKeySize
		}{
			{aes.AES128},
			{aes.AES192},
			{aes.AES256},
		}

		for _, tt := range tests {
			key, err := aes.GenerateKey(tt.keySize)
			assert.NoError(t, err)

			keyString := base64.ParseFromBytes(key)

			keyBytes, err := keyString.ToBytes()
			assert.NoError(t, err)

			plaintext := []byte("This is a test message.")

			nonce, ciphertext, verificationTag, err := aes.Encrypt(keyBytes, plaintext)
			if err != nil {
				t.Fatalf("Failed to encrypt plaintext: %v", err)
			}

			decryptedText, err := aes.Decrypt(keyBytes, nonce, ciphertext, verificationTag)
			if err != nil {
				t.Fatalf("Failed to decrypt ciphertext: %v", err)
			}

			if !bytes.Equal(plaintext, decryptedText) {
				t.Fatalf("Decrypted text does not match plaintext. Got %s, expected %s", decryptedText, plaintext)
			}
		}
	})

	t.Run("AES - encrypt and decrypt with pregenerated key 128 and base64 encoding", func(t *testing.T) {
		key := base64.Base64String("OIxi2bQPXRSNBTHWEBn4fQ")
		keyBytes, err := key.ToBytes()
		assert.NoError(t, err)

		plaintext := []byte("This is a test message.")

		nonce, ciphertext, verificationTag, err := aes.Encrypt(keyBytes, plaintext)
		if err != nil {
			t.Fatalf("Failed to encrypt plaintext: %v", err)
		}
		ciphertextBase64String := base64.ParseFromBytes(ciphertext)

		ciphertextBytes, err := ciphertextBase64String.ToBytes()
		assert.NoError(t, err)

		decryptedText, err := aes.Decrypt(keyBytes, nonce, ciphertextBytes, verificationTag)
		if err != nil {
			t.Fatalf("Failed to decrypt ciphertext: %v", err)
		}

		if !bytes.Equal(plaintext, decryptedText) {
			t.Fatalf("Decrypted text does not match plaintext. Got %s, expected %s", decryptedText, plaintext)
		}
	})

	t.Run("AES - encrypt and decrypt with pregenerated key 128 and base64 encoding", func(t *testing.T) {
		key := base64.Base64String("OIxi2bQPXRSNBTHWEBn4fQ")
		keyBytes, err := key.ToBytes()
		assert.NoError(t, err)

		plaintext := []byte("This is a test message.")

		ciphertextBytes, err := base64.Base64String("Uo8hc9YcxAX7zi0VKvTjJDwFmCPjoKpX1KPdB-kOCk4cpNNwoX_LfRlAwjjLEYEEzQ98").ToBytes()
		assert.NoError(t, err)
		nonceSize := 12
		nonce := ciphertextBytes[:nonceSize]
		authenticationTagSize := 16
		authenticationTag := ciphertextBytes[len(ciphertextBytes)-authenticationTagSize:]
		ciphertext := ciphertextBytes[nonceSize : len(ciphertextBytes)-authenticationTagSize]

		decryptedText, err := aes.Decrypt(keyBytes, nonce, ciphertext, authenticationTag)
		if err != nil {
			t.Fatalf("Failed to decrypt ciphertext: %v", err)
		}

		if !bytes.Equal(plaintext, decryptedText) {
			t.Fatalf("Decrypted text does not match plaintext. Got %s, expected %s", decryptedText, plaintext)
		}
	})

	t.Run("JWS - create default JWS document", func(t *testing.T) {
		jwsDocument, err := jws.NewDefaultDocument([]byte("this is a test message"))
		assert.NoError(t, err)

		jwsParts := strings.Split(jwsDocument, ".")
		assert.Len(t, jwsParts, 2)

		jwsHeader := jwsParts[0]
		jwsPayload := jwsParts[1]

		assert.Equal(t, "eyJhbGciOiJFUzI1NiIsInR5cCI6Imp3dCJ9", jwsHeader)
		assert.Equal(t, "dGhpcyBpcyBhIHRlc3QgbWVzc2FnZQ", jwsPayload)
	})

	t.Run("JWS - JWS document to plaintext", func(t *testing.T) {
		jwsDocument := "eyJhbGciOiJFUzI1NiIsInR5cCI6Imp3dCJ9.dGhpcyBpcyBhIHRlc3QgbWVzc2FnZQ"
		plaintext, err := jws.ToPlaintext(jwsDocument)
		assert.NoError(t, err)
		assert.Equal(t, "this is a test message", string(plaintext))
	})

	t.Run("JWS - create default signed JWS document", func(t *testing.T) {
		privateKey, err := ec.ParsePrivateKeyFromPem(privateKeyPemBytes)
		assert.NoError(t, err)
		publicKey, err := ec.ParsePublicKeyFromPem(publicKeyPemBytes)
		assert.NoError(t, err)

		jwsDocument, err := jws.NewDefaultSignedDocument([]byte("this is a test message"), privateKey)
		assert.NoError(t, err)

		jwsParts := strings.Split(jwsDocument, ".")
		assert.Len(t, jwsParts, 3)

		jwsHeader := jwsParts[0]
		jwsPayload := jwsParts[1]
		jwsSignature := jwsParts[2]

		assert.Equal(t, "eyJhbGciOiJFUzI1NiIsInR5cCI6Imp3dCJ9", jwsHeader)
		assert.Equal(t, "dGhpcyBpcyBhIHRlc3QgbWVzc2FnZQ", jwsPayload)
		assert.NotEmpty(t, jwsSignature)

		valid, err := jws.Verify(jwsDocument, publicKey)
		assert.NoError(t, err)
		assert.True(t, valid)
	})

	t.Run("JWS - JWS signed document to plaintext", func(t *testing.T) {
		jwsSignedDocument := "eyJhbGciOiJFUzI1NiIsInR5cCI6Imp3dCJ9.dGhpcyBpcyBhIHRlc3QgbWVzc2FnZQ.MEUCIG2Xb370bS4UEgWP45G-owKDhEbj_4mA2LJSXcrzkZqRAiEAg0-fBqHeMAMD2Af786HNuoQOwmbfvgHO9BbXHJmZqbw"
		plaintext, err := jws.ToPlaintext(jwsSignedDocument)
		assert.NoError(t, err)
		assert.Equal(t, "this is a test message", string(plaintext))
	})

	t.Run("JWE - create default JWE document", func(t *testing.T) {
		aesKey, err := base64.Base64String("OIxi2bQPXRSNBTHWEBn4fQ").ToBytes()
		assert.NoError(t, err)

		jweDocument, err := jwe.NewDefaultDocument(aesKey, []byte("this is a test message"))
		assert.NoError(t, err)

		jweParts := strings.Split(jweDocument, ".")
		assert.Len(t, jweParts, 5)

		header := jweParts[0]
		encryptedKey := jweParts[1]
		initializationVector := jweParts[2]
		cyphertext := jweParts[3]
		authenticationTag := jweParts[4]
		assert.Equal(t, "eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2R0NNIn0", header, "JWE header does not match expected value")
		assert.Equal(t, "", encryptedKey, "JWE encrypted key does not match expected value")
		assert.NotEmpty(t, initializationVector, "JWE initialization vector does not match expected value")
		assert.NotEmpty(t, cyphertext, "JWE cyphertext does not match expected value")
		assert.NotEmpty(t, authenticationTag, "JWE authentication tag does not match expected value")
	})

	t.Run("JWE - JWE document to plaintext", func(t *testing.T) {
		aesKey, err := base64.Base64String("OIxi2bQPXRSNBTHWEBn4fQ").ToBytes()
		assert.NoError(t, err)

		jweDocument := "eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2R0NNIn0..IX-NN0TWg9og3RvZ.ZvIVMLatje2ZLDklOuCQ8T00RiJcnw._GmIRnB7Bqshh15gEn3Ivg"
		plaintext, err := jwe.ToPlaintext(aesKey, jweDocument, jwe.DecryptUsingAES)
		assert.NoError(t, err)
		assert.Equal(t, "this is a test message", string(plaintext))
	})
}
