// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package ec_test

import (
	"crypto/elliptic"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/encoding/base64"
)

func TestGenerateKeyPair(t *testing.T) {
	privateKey, err := ec.GenerateKeyPair(elliptic.P256())
	if err != nil {
		t.Errorf("Failed to generate key pair: %v", err)
	}
	if privateKey == nil {
		t.Error("Generated key pair is nil")
	}
}

func TestVerifyKeyPair(t *testing.T) {
	tests := []struct {
		curve    elliptic.Curve
		expected bool
	}{
		{elliptic.P224(), false},
		{elliptic.P256(), true},
		{elliptic.P384(), false},
		{elliptic.P521(), false},
	}

	for _, tt := range tests {
		t.Run(tt.curve.Params().Name, func(t *testing.T) {
			privateKey, err := ec.GenerateKeyPair(tt.curve)
			assert.NoError(t, err)

			assert.Equal(t, privateKey.Verify(), tt.expected)
			assert.Equal(t, privateKey.PublicKey.Verify(), tt.expected)
		})
	}
}

func TestPrivateKeyToPem(t *testing.T) {
	privateKey, _ := ec.GenerateKeyPair(elliptic.P256())
	_, err := privateKey.ToPem()

	if err != nil {
		t.Errorf("Failed to convert private key to pem: %v", err)
	}
}

func TestPublicKeyToPem(t *testing.T) {
	pk, _ := ec.GenerateKeyPair(elliptic.P256())
	_, err := pk.PublicKey.ToPem()

	if err != nil {
		t.Errorf("Failed to convert public key to pem: %v", err)
	}
}

func TestPrintTestDataForDart(t *testing.T) {
	privateKey, err := ec.GenerateKeyPair(elliptic.P256())
	if err != nil {
		t.Errorf("Failed to generate key pair: %v", err)
	}
	if privateKey == nil {
		t.Error("Generated key pair is nil")
		return
	}

	privateKeyPem, err := privateKey.ToPem()
	if err != nil {
		t.Errorf("Failed to convert private key to pem: %v", err)
	}

	publicKeyPem, err := privateKey.PublicKey.ToPem()
	if err != nil {
		t.Errorf("Failed to convert public key to pem: %v", err)
	}

	println(string(privateKeyPem))

	println(string(publicKeyPem))
}

func TestParsePrivateKeyFromPem(t *testing.T) {
	testCases := []struct {
		name    string
		input   []byte
		isError bool
	}{
		{
			name: "Valid Private Key",
			input: []byte(`-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIOSOtUrRQ4L5cSjJIMmkhn2OQcgRqMdM+2nGpq5wHuG4oAoGCCqGSM49
AwEHoUQDQgAEbwJpbjahL3UYOUu18HXE7ZT6ULZT+gdpqEoB8k4+HiryDhH8ON5Z
Yb0quJ1JZRx7hWBChMAcRz3cYVTYp8YbeQ==
-----END EC PRIVATE KEY-----`),
			isError: false,
		},
		{
			name:    "Invalid Private Key",
			input:   []byte("-----BEGIN RSA PRIVATE KEY-----\nsome_random_key\n-----END RSA PRIVATE KEY-----\n"),
			isError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			_, err := ec.ParsePrivateKeyFromPem(tc.input)
			if (err != nil) != tc.isError {
				t.Errorf("ParsePrivateKeyFromPem() error = %v, wantErr %v", err, tc.isError)
			}
		})
	}
}

func TestParsePublicKeyFromPem(t *testing.T) {
	testCases := []struct {
		name    string
		input   []byte
		isError bool
	}{
		{
			name: "Valid Public Key",
			input: []byte(`-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEbwJpbjahL3UYOUu18HXE7ZT6ULZT
+gdpqEoB8k4+HiryDhH8ON5ZYb0quJ1JZRx7hWBChMAcRz3cYVTYp8YbeQ==
-----END PUBLIC KEY-----`),
			isError: false,
		},
		{
			name:    "Invalid Public Key",
			input:   []byte("-----BEGIN RSA PUBLIC KEY-----\nsome_random_key\n-----END RSA PUBLIC KEY-----\n"),
			isError: true,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			_, err := ec.ParsePublicKeyFromPem(tc.input)
			if (err != nil) != tc.isError {
				t.Errorf("ParsePublicKeyFromPem() error = %v, wantErr %v", err, tc.isError)
			}
		})
	}
}

func TestPublicKeyToBase64(t *testing.T) {
	pk, _ := ec.GenerateKeyPair(elliptic.P256())
	_, err := pk.PublicKey.ToBase64()

	if err != nil {
		t.Errorf("Failed to convert public key to base64: %v", err)
	}
}

func TestParsePublicKeyFromBase64(t *testing.T) {
	testCases := []struct {
		name    string
		input   base64.Base64String
		isError bool
	}{
		{
			name:    "Valid Public Key",
			input:   base64.Base64String("MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEEqKRT8Gu2gCJS7cjG0MuQds-qzZZYYLooIT0_I1HhuHSqexpyxWt51F91HOKcWLhg1RiTPBA2LUqot2mCf7a3g"),
			isError: false,
		},
		{
			name:    "Invalid Public Key",
			input:   "some_random_key",
			isError: true,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			_, err := ec.ParsePublicKeyFromBase64(tc.input)
			if (err != nil) != tc.isError {
				t.Errorf("ParsePublicKeyFromBase64() error = %v, wantErr %v", err, tc.isError)
			}
		})
	}
}
