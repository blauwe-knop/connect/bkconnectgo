// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package ec

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"math/big"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/encoding/base64"
)

type PublicKey struct {
	elliptic.Curve
	X, Y *big.Int
}

type PrivateKey struct {
	PublicKey
	D *big.Int
}

func GenerateKeyPair(curve elliptic.Curve) (*PrivateKey, error) {
	ecdsaPrivateKey, err := ecdsa.GenerateKey(curve, rand.Reader)
	if err != nil {
		return nil, err
	}

	return importEcdsaPrivateKey(*ecdsaPrivateKey), nil
}

func ParsePrivateKeyFromPem(privateKeyPem []byte) (*PrivateKey, error) {
	blockPrivateKey, _ := pem.Decode(privateKeyPem)
	if blockPrivateKey == nil {
		return nil, fmt.Errorf("failed to parse private key")
	}

	ecdsaPrivateKey, err := x509.ParseECPrivateKey(blockPrivateKey.Bytes)
	if err != nil {
		return nil, err
	}

	return importEcdsaPrivateKey(*ecdsaPrivateKey), nil
}

func ParsePublicKeyFromPem(publicKeyPem []byte) (*PublicKey, error) {
	blockPublicKey, _ := pem.Decode(publicKeyPem)
	if blockPublicKey == nil {
		return nil, fmt.Errorf("failed to parse public key")
	}

	genericPublicKey, err := x509.ParsePKIXPublicKey(blockPublicKey.Bytes)
	if err != nil {
		return nil, err
	}

	ecdsaPublicKey, ok := genericPublicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("wrong public key type")
	}

	return importEcdsaPublicKey(*ecdsaPublicKey), nil
}

func (pk *PrivateKey) ToPem() ([]byte, error) {
	ecdsaPrivateKey := pk.ToEcdsa()

	encodedPrivateKey, err := x509.MarshalECPrivateKey(ecdsaPrivateKey)
	if err != nil {
		return nil, err
	}

	pemEncodedPrivateKey := pem.EncodeToMemory(
		&pem.Block{
			Type:  "EC PRIVATE KEY",
			Bytes: encodedPrivateKey,
		},
	)

	return pemEncodedPrivateKey, nil
}

func (pk *PublicKey) ToPem() ([]byte, error) {
	ecdsaPublicKey := pk.ToEcdsa()

	encodedPublicKey, err := x509.MarshalPKIXPublicKey(ecdsaPublicKey)
	if err != nil {
		return nil, err
	}

	pemEncodedPublicKey := pem.EncodeToMemory(
		&pem.Block{
			Type:  "PUBLIC KEY",
			Bytes: encodedPublicKey,
		},
	)

	return pemEncodedPublicKey, nil
}

func (pk *PublicKey) ToBase64() (base64.Base64String, error) {
	ecdsaPublicKey := pk.ToEcdsa()

	encodedPublicKey, err := x509.MarshalPKIXPublicKey(ecdsaPublicKey)
	if err != nil {
		return "", err
	}

	return base64.ParseFromBytes(encodedPublicKey), nil
}

func ParsePublicKeyFromBase64(publicKeyBase64 base64.Base64String) (*PublicKey, error) {
	decodedPublicKey, err := base64.Base64String.ToBytes(publicKeyBase64)
	if err != nil {
		return nil, fmt.Errorf("failed to decode base64 public key: %w", err)
	}

	genericPublicKey, err := x509.ParsePKIXPublicKey(decodedPublicKey)
	if err != nil {
		return nil, fmt.Errorf("failed to parse public key: %w", err)
	}

	ecdsaPublicKey, ok := genericPublicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("wrong public key type")
	}

	return importEcdsaPublicKey(*ecdsaPublicKey), nil
}

func (pk *PrivateKey) Verify() bool {
	switch name := pk.Params().Name; name {
	case "P-256":
		return true
	default:
		return false
	}
}

func (pk *PublicKey) Verify() bool {
	switch name := pk.Params().Name; name {
	case "P-256":
		return true
	default:
		return false
	}
}

func (pk *PrivateKey) ToEcdsa() *ecdsa.PrivateKey {
	return &ecdsa.PrivateKey{
		PublicKey: ecdsa.PublicKey{
			Curve: pk.PublicKey.Curve,
			X:     pk.PublicKey.X,
			Y:     pk.PublicKey.Y,
		},
		D: pk.D,
	}
}

func (pk *PublicKey) ToEcdsa() *ecdsa.PublicKey {
	return &ecdsa.PublicKey{
		Curve: pk.Curve,
		X:     pk.X,
		Y:     pk.Y,
	}
}

func importEcdsaPrivateKey(ecdsaPrivateKey ecdsa.PrivateKey) *PrivateKey {
	return &PrivateKey{
		PublicKey: PublicKey{
			Curve: ecdsaPrivateKey.PublicKey.Curve,
			X:     ecdsaPrivateKey.PublicKey.X,
			Y:     ecdsaPrivateKey.PublicKey.Y,
		},
		D: ecdsaPrivateKey.D,
	}
}

func importEcdsaPublicKey(ecdsaPublicKey ecdsa.PublicKey) *PublicKey {
	return &PublicKey{
		Curve: ecdsaPublicKey.Curve,
		X:     ecdsaPublicKey.X,
		Y:     ecdsaPublicKey.Y,
	}
}
