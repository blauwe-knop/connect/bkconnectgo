// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package aes_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/aes"
)

func TestGenerateKey(t *testing.T) {
	tests := []struct {
		keySize aes.AESKeySize
	}{
		{aes.AES128},
		{aes.AES192},
		{aes.AES256},
	}

	for _, tt := range tests {
		t.Run(string(rune(tt.keySize)), func(t *testing.T) {
			key, err := aes.GenerateKey(tt.keySize)
			if err != nil {
				t.Fatalf("Failed to generate key: %v", err)
			}
			if len(key) != int(tt.keySize) {
				t.Fatalf("Expected key length %d, got %d", tt.keySize, len(key))
			}
		})
	}
}

func TestEncryptDecrypt(t *testing.T) {
	tests := []struct {
		keySize aes.AESKeySize
	}{
		{aes.AES128},
		{aes.AES192},
		{aes.AES256},
	}

	for _, tt := range tests {
		t.Run(string(rune(tt.keySize)), func(t *testing.T) {
			key, err := aes.GenerateKey(tt.keySize)
			if err != nil {
				t.Fatalf("Failed to generate key: %v", err)
			}

			plaintext := []byte("This is a test message.")
			nonce, ciphertext, authenticationTag, err := aes.Encrypt(key, plaintext)
			if err != nil {
				t.Fatalf("Failed to encrypt plaintext: %v", err)
			}

			decryptedText, err := aes.Decrypt(key, nonce, ciphertext, authenticationTag)
			if err != nil {
				t.Fatalf("Failed to decrypt ciphertext: %v", err)
			}

			if !bytes.Equal(plaintext, decryptedText) {
				t.Fatalf("Decrypted text does not match plaintext. Got %s, expected %s", decryptedText, plaintext)
			}
		})
	}
}

func TestVerifyKey(t *testing.T) {
	aesKey128, err := aes.GenerateKey(aes.AES128)
	assert.NoError(t, err)

	aesKey192, err := aes.GenerateKey(aes.AES192)
	assert.NoError(t, err)

	aesKey256, err := aes.GenerateKey(aes.AES256)
	assert.NoError(t, err)

	tooShortKey := []byte("too short")

	aes256CtrKey := []byte("4nEU6+BoSmT40isMAD83YA/JlygYBnD3nENbM4SNChg=")

	tests := []struct {
		name       string
		key        []byte
		shouldPass bool
	}{
		{"Valid AES128 Key", aesKey128, true},
		{"Valid AES192 Key", aesKey192, true},
		{"Valid AES256 Key", aesKey256, true},
		{"Invalid Key Size", tooShortKey, false},
		{"Invalid Key Data", aes256CtrKey, false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err != nil {
				t.Fatalf("Failed to generate key: %v", err)
			}

			result := aes.VerifyKey(tt.key)
			if result != tt.shouldPass {
				t.Errorf("VerifyKey() = %v, want %v", result, tt.shouldPass)
			}
		})
	}
}
