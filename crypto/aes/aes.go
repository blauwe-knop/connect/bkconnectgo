// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package aes

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"fmt"
	"io"
)

type AESKeySize int

const (
	AES128 AESKeySize = 16
	AES192 AESKeySize = 24
	AES256 AESKeySize = 32
)

// GenerateKey generates a random key for AES 128, 192 or 256
func GenerateKey(keySize AESKeySize) ([]byte, error) {
	key := make([]byte, keySize)
	if _, err := rand.Read(key); err != nil {
		return nil, err
	}

	return key, nil
}

func VerifyKey(key []byte) bool {
	if len(key) < int(AES128) {
		return false
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return false
	}

	_, err = cipher.NewGCM(block)
	return err == nil
}

// Encrypt encrypts plaintext using AES in GCM mode
// and returns a nonce, ciphertext and authentication tag or an error
func Encrypt(key []byte, plaintext []byte) (nonce []byte, ciphertext []byte, authenticationTag []byte, error error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, nil, nil, err
	}

	gcmCipher, err := cipher.NewGCM(block)
	if err != nil {
		return nil, nil, nil, err
	}

	nonce = make([]byte, gcmCipher.NonceSize())
	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, nil, nil, err
	}

	sealedText := gcmCipher.Seal(nil, nonce, plaintext, nil)
	authenticationTagLength := 16
	ciphertext = sealedText[:len(sealedText)-authenticationTagLength]
	authenticationTag = sealedText[len(sealedText)-authenticationTagLength:]

	return nonce, ciphertext, authenticationTag, nil
}

// Decrypt decrypts ciphertext using AES in GCM mode
// and returns the decrypted text as plain text or an error
func Decrypt(key []byte, nonce []byte, ciphertext []byte, authenticationTag []byte) (plaintext []byte, error error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcmCipher, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	nonceSize := len(nonce)

	if len(ciphertext)+len(authenticationTag) < nonceSize {
		return nil, fmt.Errorf("ciphertext too short")
	}

	plaintext, err = gcmCipher.Open(nil, nonce, append(ciphertext, authenticationTag...), nil)
	if err != nil {
		return nil, err
	}

	return plaintext, nil
}
