// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package ecdsa_test

import (
	"crypto/elliptic"
	"testing"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ecdsa"
)

func TestSignAndVerify(t *testing.T) {
	// Generate a new ECDSA key pair
	privateKey, err := ec.GenerateKeyPair(elliptic.P256())
	if err != nil {
		t.Fatalf("Failed to generate ECDSA key pair: %v", err)
	}

	publicKey := &privateKey.PublicKey

	// Convert to custom ec key types
	privateEcKey := &ec.PrivateKey{
		PublicKey: ec.PublicKey{
			Curve: publicKey.Curve,
			X:     publicKey.X,
			Y:     publicKey.Y,
		},
		D: privateKey.D,
	}

	publicEcKey := &ec.PublicKey{
		Curve: publicKey.Curve,
		X:     publicKey.X,
		Y:     publicKey.Y,
	}

	// Define the message to sign
	message := []byte("Hello, ECDSA!")

	// Sign the message
	signature, err := ecdsa.Sign(privateEcKey, message)
	if err != nil {
		t.Fatalf("Failed to sign message: %v", err)
	}

	// Verify the signature
	valid := ecdsa.Verify(publicEcKey, message, signature)
	if !valid {
		t.Errorf("Signature verification failed")
	}

	// Test with a modified message
	modifiedMessage := []byte("Hello, ECDSA (modified)!")
	valid = ecdsa.Verify(publicEcKey, modifiedMessage, signature)
	if valid {
		t.Errorf("Signature verification should have failed with modified message")
	}

	// Test with a modified signature
	modifiedSignature := make([]byte, len(signature))
	copy(modifiedSignature, signature)
	modifiedSignature[0] ^= 0xFF // Invert the first byte
	valid = ecdsa.Verify(publicEcKey, message, modifiedSignature)
	if valid {
		t.Errorf("Signature verification should have failed with modified signature")
	}
}
