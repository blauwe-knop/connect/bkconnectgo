// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package ecdsa

import (
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha256"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ec"
)

func Sign(privateKey *ec.PrivateKey, message []byte) ([]byte, error) {
	hash := sha256.Sum256(message)

	ecdsaPrivateKey := &ecdsa.PrivateKey{
		PublicKey: ecdsa.PublicKey{
			Curve: privateKey.PublicKey.Curve,
			X:     privateKey.PublicKey.X,
			Y:     privateKey.PublicKey.Y,
		},
		D: privateKey.D,
	}

	return ecdsa.SignASN1(rand.Reader, ecdsaPrivateKey, hash[:])
}

func Verify(publicKey *ec.PublicKey, message []byte, signature []byte) bool {
	hash := sha256.Sum256(message)

	ecdsaPublicKey := &ecdsa.PublicKey{
		Curve: publicKey.Curve,
		X:     publicKey.X,
		Y:     publicKey.Y,
	}

	return ecdsa.VerifyASN1(ecdsaPublicKey, hash[:], signature)
}
