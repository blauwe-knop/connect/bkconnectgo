// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package ecies_test

import (
	"crypto/elliptic"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ecies"
)

func TestEncrypt(t *testing.T) {
	privateKey, err := ec.GenerateKeyPair(elliptic.P256())
	if err != nil {
		t.Fatalf("Failed to generate ECDSA key pair: %v", err)
	}

	tests := []struct {
		name      string
		plainText []byte
	}{
		{
			name:      "ValidData",
			plainText: []byte("Hello World"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			nonce, ciphertext, authenticationTag, err := ecies.Encrypt(&privateKey.PublicKey, tt.plainText)
			assert.NoError(t, err)
			assert.NotEmpty(t, nonce)
			assert.NotEmpty(t, ciphertext)
			assert.NotEmpty(t, authenticationTag)
		})
	}
}

func TestDecrypt(t *testing.T) {
	privateKey, _ := ec.GenerateKeyPair(ecies.P256)

	nonce, ciphertext, authenticationTag, err := ecies.Encrypt(&privateKey.PublicKey, []byte("Hello World"))
	assert.NoError(t, err)

	tests := []struct {
		name              string
		nonce             []byte
		ciphertext        []byte
		authenticationTag []byte
	}{
		{
			name:              "ValidData",
			nonce:             nonce,
			ciphertext:        ciphertext,
			authenticationTag: authenticationTag,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			decrypted, err := ecies.Decrypt(privateKey, tt.nonce, tt.ciphertext, tt.authenticationTag)
			assert.NoError(t, err)
			assert.Equal(t, "Hello World", string(decrypted))
		})
	}
}
