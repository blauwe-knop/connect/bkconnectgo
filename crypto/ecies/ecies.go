// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package ecies

import (
	"crypto/elliptic"
	"crypto/rand"
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/crypto"
	eth_ecies "github.com/ethereum/go-ethereum/crypto/ecies"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ec"
)

func Encrypt(publicKey *ec.PublicKey, plainText []byte) (nonce []byte, ciphertext []byte, authenticationTag []byte, error error) {
	eciesPublicKey := &eth_ecies.PublicKey{
		Curve:  P256,
		X:      publicKey.X,
		Y:      publicKey.Y,
		Params: eth_ecies.ECIES_AES128_SHA256,
	}

	cipherText, err := eth_ecies.Encrypt(rand.Reader, eciesPublicKey, plainText, nil, nil)
	if err != nil {
		return nil, nil, nil, err
	}

	if len(cipherText) < 28 {
		return nil, nil, nil, fmt.Errorf("ciphertext is too short to contain nonce and authentication tag")
	}
	nonceLength := 16
	authenticationTagLength := 16
	nonce = cipherText[:nonceLength]
	authenticationTag = cipherText[len(cipherText)-authenticationTagLength:]
	cipherText = cipherText[nonceLength : len(cipherText)-authenticationTagLength]

	return nonce, cipherText, authenticationTag, nil
}

func Decrypt(privateKey *ec.PrivateKey, nonce []byte, ciphertext []byte, authenticationTag []byte) (plaintext []byte, error error) {
	eciesPrivateKey := &eth_ecies.PrivateKey{
		PublicKey: eth_ecies.PublicKey{
			Curve:  P256,
			X:      privateKey.PublicKey.X,
			Y:      privateKey.PublicKey.Y,
			Params: eth_ecies.ECIES_AES128_SHA256,
		},
		D: privateKey.D,
	}

	combinedCiphertext := append(append(nonce, ciphertext...), authenticationTag...)
	return eciesPrivateKey.Decrypt(combinedCiphertext, nil, nil)
}

// Curve wraps elliptic.Curve from the Go standard library to implement the crypto.EllipticCurve interface
// from github.com/ethereum/go-ethereum/crypto, which is implicitly required by the Encrypt() and Decrypt()
// functions from github.com/ethereum/go-ethereum/crypto/ecies.
type Curve struct {
	elliptic.Curve
}

func (c Curve) Marshal(x, y *big.Int) []byte {
	// nolint:staticcheck
	return elliptic.Marshal(c.Curve, x, y)
}

func (c Curve) Unmarshal(data []byte) (x, y *big.Int) {
	// nolint:staticcheck
	return elliptic.Unmarshal(c.Curve, data)
}

var P256 = Curve{
	Curve: elliptic.P256(),
}

// Assert that P256 supports the interface that the Encrypt and Decrypt functions from
// github.com/ethereum/go-ethereum/crypto/ecies implicitly require.
var _ crypto.EllipticCurve = P256
