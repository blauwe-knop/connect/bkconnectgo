// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package jwe

import (
	"crypto/elliptic"
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ecies"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/jwt/jwk"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/jwt"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/aes"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/encoding/base64"
)

type DecryptFunction func(key interface{}, encryptedKey []byte, initializationVector []byte, ciphertext []byte, authenticationTag []byte) (plaintext []byte, err error)
type EncryptFunction func(key interface{}, plaintext []byte) (encryptedKey []byte, initializationVector []byte, ciphertext []byte, authenticationTag []byte, err error)

func DecryptUsingAES(aesKey interface{}, _ []byte, initializationVector []byte, ciphertext []byte, authenticationTag []byte) ([]byte, error) {
	if len(aesKey.([]byte)) < int(aes.AES128) {
		return nil, fmt.Errorf("AES key length is less than 128 bits, we currently only support AES128 and upwards. Provided AES key length is %d bits", len(aesKey.([]byte))*8)
	}
	plaintext, err := aes.Decrypt(aesKey.([]byte), initializationVector, ciphertext, authenticationTag)
	if err != nil {
		return nil, err
	}

	return plaintext, nil
}

func DecryptUsingEC(ecPrivateKey interface{}, _ []byte, iv []byte, ciphertext []byte, authenticationTag []byte) ([]byte, error) {
	plaintext, err := ecies.Decrypt(ecPrivateKey.(*ec.PrivateKey), iv, ciphertext, authenticationTag)
	if err != nil {
		return nil, err
	}

	return plaintext, nil
}

func EncryptUsingAES(aesKey interface{}, ciphertext []byte) (encryptedKey []byte, initializationVector []byte, ciphertextAsBase64Encoded []byte, authenticationTag []byte, err error) {
	if len(aesKey.([]byte)) < int(aes.AES128) {
		return nil, nil, nil, nil, fmt.Errorf("AES key length is less than 128 bits, we currently only support AES128 and upwards. Provided AES key length is %d bits", len(aesKey.([]byte))*8)
	}
	nonce, encryptedCiphertext, authenticationTag, err := aes.Encrypt(aesKey.([]byte), ciphertext)
	if err != nil {
		return nil, nil, nil, nil, fmt.Errorf("error occurred while encrypting JWE Ciphertext using AES key. Error: %v", err)
	}
	return nil, nonce, encryptedCiphertext, authenticationTag, nil
}

func EncryptUsingEC(ecPublicKey interface{}, ciphertext []byte) (encryptedKey []byte, initializationVector []byte, ciphertextAsBase64Encoded []byte, authenticationTag []byte, err error) {
	nonce, encryptedCiphertext, authenticationTag, err := ecies.Encrypt(ecPublicKey.(*ec.PublicKey), ciphertext)
	if err != nil {
		return nil, nil, nil, nil, fmt.Errorf("error occurred while encrypting JWE Ciphertext using ECDH-ES key. Error: %v", err)
	}
	return nil, nonce, encryptedCiphertext, authenticationTag, nil
}

func EncryptUsingECWithJWK(jwkDocument interface{}, ciphertext []byte) (encryptedKey []byte, initializationVector []byte, ciphertextAsBase64Encoded []byte, authenticationTag []byte, err error) {
	jwkDocumentBytes := jwkDocument.([]byte)
	var jwkPayload jwk.Document
	err = json.Unmarshal(jwkDocumentBytes, &jwkPayload)
	if err != nil {
		return nil, nil, nil, nil, fmt.Errorf("error occurred while encrypting JWE Ciphertext using ECDH-ES key with JWK. Error: %v", err)
	}

	key := &ec.PublicKey{
		Curve: elliptic.P256(),
		X:     jwkPayload.X,
		Y:     jwkPayload.Y,
	}
	nonce, encryptedCiphertext, authenticationTag, err := ecies.Encrypt(key, ciphertext)
	if err != nil {
		return nil, nil, nil, nil, fmt.Errorf("error occurred while encrypting JWE Ciphertext using ECDH-ES key. Error: %v", err)
	}
	return nil, nonce, encryptedCiphertext, authenticationTag, nil
}

// NewDefaultDocument - takes a plaintext and returns a Base64 encoded and AES encrypted
// JWE Document with a default JWE Header.
func NewDefaultDocument(aesKey []byte, plaintext []byte) (string, error) {
	header := defaultHeader()
	return NewDocument(aesKey, plaintext, header.Encryption, HeaderAlg(header.Algorithm), EncryptUsingAES)
}

// NewDocument - takes a plaintext, encryption and algorithm and returns a Base64 encoded and
// encrypted (using provided encryption function) JWE Document.
func NewDocument(key interface{}, plaintext []byte, enc HeaderEnc, alg HeaderAlg, encryptFunction EncryptFunction) (string, error) {
	header := newHeader(enc, alg)
	encryptedKey, initializationVector, ciphertext, authenticationTag, err := bodyPartsFromString(key, plaintext, encryptFunction)
	if err != nil {
		return "", err
	}
	document := newDocument(header, encryptedKey, initializationVector, ciphertext, authenticationTag)
	documentAsString, err := document.asEncodedString()
	if err != nil {
		return "", err
	}
	return documentAsString, nil
}

// NewDocumentWithEPK - takes a plaintext, encryption (enc), algorithm (alg), ephemeral public key and returns a Base64 encoded
// and encrypted (using provided encryption function) JWE Document.
// The ephemeral public key is added to the JWE header "epk" field.
func NewDocumentWithEPK(plaintext []byte, enc HeaderEnc, alg HeaderAlg, ephemeralPublicKey []byte, encryptFunction EncryptFunction) (string, error) {
	header := newHeaderWithEPK(enc, alg, ephemeralPublicKey)
	encryptedKey, initializationVector, ciphertext, authenticationTag, err := bodyPartsFromString(ephemeralPublicKey, plaintext, encryptFunction)
	if err != nil {
		return "", err
	}
	document := newDocument(header, encryptedKey, initializationVector, ciphertext, authenticationTag)
	documentAsString, err := document.asEncodedString()
	if err != nil {
		return "", err
	}
	return documentAsString, nil
}

// ToPlaintext - takes a Base64 encoded, encrypted JWE Document and returns the
// decrypted (using provided decryption function) ciphertext as plaintext.
func ToPlaintext(key interface{}, jweDocument string, decryptFunction DecryptFunction) ([]byte, error) {
	documentParts := strings.Split(jweDocument, ".")
	if len(documentParts) != 5 {
		return nil, fmt.Errorf("JWE Document string provided with %d parts, but JWE Document string is expected with 5 parts", len(documentParts))
	}

	encryptedKey, err := encryptedKeyFromEncodedString(base64.Base64String(documentParts[1]))
	if err != nil {
		return nil, err
	}

	initializationVector, err := initializationVectorFromEncodedString(base64.Base64String(documentParts[2]))
	if err != nil {
		return nil, err
	}

	ciphertext, err := ciphertextFromEncryptedEncodedString(base64.Base64String(documentParts[3]))
	if err != nil {
		return nil, err
	}

	authenticationTag, err := authenticationTagFromEncodedString(base64.Base64String(documentParts[4]))
	if err != nil {
		return nil, err
	}

	plaintext, err := decryptFunction(key, encryptedKey.asBytes(), initializationVector.asBytes(), ciphertext.asBytes(), authenticationTag.asBytes())
	if err != nil {
		return nil, err
	}

	return plaintext, nil
}

// Document - the JWE specific document.
// Contains a JWE header, encrypted key, initialization vector, ciphertext and authentication tag, seperated by a dot.
type Document struct {
	Header               *Header
	EncryptedKey         EncryptedKey
	InitializationVector InitializationVector
	Ciphertext           Ciphertext
	AuthenticationTag    AuthenticationTag
}

func newDocument(header *Header, encryptedKey EncryptedKey, initializationVector InitializationVector, ciphertext Ciphertext, authenticationTag AuthenticationTag) *Document {
	return &Document{
		Header:               header,
		EncryptedKey:         encryptedKey,
		InitializationVector: initializationVector,
		Ciphertext:           ciphertext,
		AuthenticationTag:    authenticationTag,
	}
}

// asEncodedString - The value returned is itself not Base64 encoded, but its parts are Base64 encoded, seperated by a "."
func (d *Document) asEncodedString() (string, error) {
	encodedHeaderAsJson, err := d.Header.asEncodedString()
	if err != nil {
		return "", err
	}
	encodedEncryptionKey := d.EncryptedKey.asEncodedString()
	encodedInitializationVector := d.InitializationVector.asEncodedString()
	encodedCiphertext := d.Ciphertext.asEncodedString()
	encodedAuthenticationTag := d.AuthenticationTag.asEncodedString()

	return fmt.Sprintf("%s.%s.%s.%s.%s", encodedHeaderAsJson, encodedEncryptionKey, encodedInitializationVector, encodedCiphertext, encodedAuthenticationTag), nil
}

func bodyPartsFromString(key interface{}, plaintext []byte, encryptFunction EncryptFunction) (EncryptedKey, InitializationVector, Ciphertext, AuthenticationTag, error) {
	_, nonce, encryptedCiphertext, authenticationTag, err := encryptFunction(key, plaintext)
	if err != nil {
		return nil, nil, nil, nil, fmt.Errorf("error occurred while encrypting JWE Ciphertext using provided key and encryption function. Error: %v", err)
	}
	return nil, nonce, encryptedCiphertext, authenticationTag, nil
}

// Header - the JWE specific header.
type Header struct {
	*jwt.JOSEHeader
	Encryption          HeaderEnc `json:"enc"`
	EphemeralPublicKey  string    `json:"epk,omitempty"`
	AgreementPartyUInfo string    `json:"apu,omitempty"`
	AgreementPartyVInfo string    `json:"apv,omitempty"`
}

type HeaderEnc string
type HeaderAlg string

const (
	A128GCM HeaderEnc = "A128GCM"
	A192GCM HeaderEnc = "A192GCM"
	A256GCM HeaderEnc = "A256GCM"
)
const (
	DIR    HeaderAlg = "dir"
	ECDHES HeaderAlg = "ECDH-ES"
)

func defaultHeader() *Header {
	return &Header{
		JOSEHeader: &jwt.JOSEHeader{
			Algorithm: string(DIR),
		},
		Encryption: A256GCM,
	}
}

func newHeader(enc HeaderEnc, alg HeaderAlg) *Header {
	return &Header{
		JOSEHeader: &jwt.JOSEHeader{
			Algorithm: string(alg),
		},
		Encryption: enc,
	}
}

func newHeaderWithEPK(enc HeaderEnc, alg HeaderAlg, ephemeralPublicKey []byte) *Header {
	return &Header{
		JOSEHeader: &jwt.JOSEHeader{
			Algorithm: string(alg),
		},
		Encryption:         enc,
		EphemeralPublicKey: string(ephemeralPublicKey),
	}
}

func (h *Header) asEncodedString() (base64.Base64String, error) {
	headerAsString, err := h.asString()
	if err != nil {
		return "", err
	}
	return base64.ParseFromBytes([]byte(headerAsString)), nil
}

func (h *Header) asString() (string, error) {
	headerAsJson, err := json.Marshal(h)
	if err != nil {
		return "", fmt.Errorf("error occurred while marshalling JWE Header. Error: %v", err)
	}
	return string(headerAsJson), nil
}

// EncryptedKey - the JWE encrypted key.
type EncryptedKey []byte

func encryptedKeyFromEncodedString(encodedEncryptedKey base64.Base64String) (*EncryptedKey, error) {
	encryptedKey, err := encodedEncryptedKey.ToBytes()
	if err != nil {
		return nil, fmt.Errorf("error occurred while Base64 decoding JWE encrypted key. Error: %v", err)
	}

	return encryptedKeyFromString(encryptedKey), nil
}

func encryptedKeyFromString(encryptedKeyAsString []byte) *EncryptedKey {
	encryptedKey := EncryptedKey(encryptedKeyAsString)
	return &encryptedKey
}

func (k *EncryptedKey) asEncodedString() base64.Base64String {
	encryptedKeyAsString := k.asBytes()
	return base64.ParseFromBytes(encryptedKeyAsString)
}

func (k *EncryptedKey) asBytes() []byte {
	return *k
}

// InitializationVector - the JWE initialization vector.
type InitializationVector []byte

func initializationVectorFromEncodedString(encodedInitializationVector base64.Base64String) (*InitializationVector, error) {
	initializationVector, err := encodedInitializationVector.ToBytes()
	if err != nil {
		return nil, fmt.Errorf("error occurred while Base64 decoding JWE initialization vector. Error: %v", err)
	}

	return initializationVectorFromString(initializationVector), nil
}

func initializationVectorFromString(initializationVectorAsString []byte) *InitializationVector {
	initializationVector := InitializationVector(initializationVectorAsString)
	return &initializationVector
}

func (v *InitializationVector) asEncodedString() base64.Base64String {
	initializationVectorAsString := v.asBytes()
	return base64.ParseFromBytes(initializationVectorAsString)
}

func (v *InitializationVector) asBytes() []byte {
	return *v
}

// Ciphertext - the JWE specific ciphertext.
// The JWE ciphertext is always AES encrypted and Base64 decoded internally.
// Externally it can be exposed as:
// - AES decrypted and Base64 decoded string
// - AES decrypted and Base64 encoded string
type Ciphertext []byte

func (p *Ciphertext) asEncodedString() base64.Base64String {
	return base64.ParseFromBytes(p.asBytes())
}

func (p Ciphertext) asBytes() []byte {
	return p
}

func ciphertextFromEncryptedEncodedString(encryptedEncodedCiphertextString base64.Base64String) (Ciphertext, error) {
	ciphertextString, err := encryptedEncodedCiphertextString.ToBytes()
	if err != nil {
		return nil, fmt.Errorf("error occurred while Base64 decoding JWE Ciphertext using AES key. Error: %v", err)
	}
	return ciphertextFromEncryptedString(string(ciphertextString[:])), nil
}

func ciphertextFromEncryptedString(encryptedCiphertextString string) Ciphertext {
	return Ciphertext(encryptedCiphertextString)
}

// AuthenticationTag - the JWE authentication tag.
type AuthenticationTag []byte

func authenticationTagFromEncodedString(encodedAuthenticationTag base64.Base64String) (*AuthenticationTag, error) {
	authenticationTag, err := encodedAuthenticationTag.ToBytes()
	if err != nil {
		return nil, fmt.Errorf("error occurred while Base64 decoding JWE authentication tag. Error: %v", err)
	}

	return authenticationTagFromString(authenticationTag), nil
}

func authenticationTagFromString(authenticationTagAsString []byte) *AuthenticationTag {
	authenticationTag := AuthenticationTag(authenticationTagAsString)
	return &authenticationTag
}

func (t *AuthenticationTag) asEncodedString() base64.Base64String {
	authenticationTagAsString := t.asBytes()
	return base64.ParseFromBytes(authenticationTagAsString)
}

func (t *AuthenticationTag) asBytes() []byte {
	return *t
}
