package jwe

import (
	"crypto/elliptic"
	"encoding/json"
	"fmt"
	"reflect"
	"slices"
	"strings"
	"testing"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/jwt/jwk"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/jwt"

	"github.com/stretchr/testify/assert"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/aes"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/encoding/base64"
)

func TestPlaintextToDocumentToPlaintext(t *testing.T) {
	sessionAesKey, err := aes.GenerateKey(aes.AES256)
	assert.NoError(t, err)
	type args struct {
		aesKey    []byte
		plaintext []byte
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "ValidPlaintextToDocumentToPlaintext",
			args: args{
				aesKey:    sessionAesKey,
				plaintext: []byte("some-text"),
			},
			want:    []byte("some-text"),
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			document, err := NewDefaultDocument(tt.args.aesKey, tt.args.plaintext)
			if !tt.wantErr(t, err, fmt.Sprintf("NewDefaultDocument(%v, %v)", tt.args.aesKey, tt.args.plaintext)) {
				return
			}
			plaintext, err := ToPlaintext(tt.args.aesKey, document, DecryptUsingAES)
			if !tt.wantErr(t, err, fmt.Sprintf("ToPlaintext(%v, %v)", tt.args.aesKey, document)) {
				return
			}
			assert.Equalf(t, tt.want, plaintext, "NewDefaultDocument(%v, %v), ToPlaintext(%v, %v)", tt.args.aesKey, tt.args.plaintext, tt.args.aesKey, document)
		})
	}
}

func TestPlaintextToDocumentToPlaintextUsingEC(t *testing.T) {
	ecKeyPair, err := ec.GenerateKeyPair(elliptic.P256())
	assert.NoError(t, err)
	type args struct {
		ecPublicKey  *ec.PublicKey
		ecPrivateKey *ec.PrivateKey
		plaintext    []byte
		enc          HeaderEnc
		alg          HeaderAlg
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "ValidPlaintextToDocumentToPlaintextUsingEC",
			args: args{
				ecPublicKey:  &ecKeyPair.PublicKey,
				ecPrivateKey: ecKeyPair,
				plaintext:    []byte("some-text"),
				enc:          A128GCM,
				alg:          ECDHES,
			},
			want:    []byte("some-text"),
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			document, err := NewDocument(tt.args.ecPublicKey, tt.args.plaintext, tt.args.enc, tt.args.alg, EncryptUsingEC)
			if !tt.wantErr(t, err, fmt.Sprintf("NewDefaultDocument(%v, %v, %v, %v)", tt.args.ecPublicKey, tt.args.plaintext, tt.args.enc, tt.args.alg)) {
				return
			}
			plaintext, err := ToPlaintext(tt.args.ecPrivateKey, document, DecryptUsingEC)
			if !tt.wantErr(t, err, fmt.Sprintf("ToPlaintext(%v, %v)", tt.args.ecPrivateKey, document)) {
				return
			}
			assert.Equalf(t, tt.want, plaintext, "NewDocument(%v, %v, %v, %v), ToPlaintext(%v, %v)", tt.args.ecPublicKey, tt.args.plaintext, tt.args.enc, tt.args.alg, tt.args.ecPrivateKey, document)
		})
	}
}

func TestPlaintextToDocumentToPlaintextUsingECAndJWK(t *testing.T) {
	ecKeyPair, err := ec.GenerateKeyPair(elliptic.P256())
	assert.NoError(t, err)
	jwkDocument := &jwk.Document{
		KeyType: jwk.EC,
		Curve:   ecKeyPair.PublicKey.Curve.Params().Name,
		X:       ecKeyPair.PublicKey.X,
		Y:       ecKeyPair.PublicKey.Y,
		D:       ecKeyPair.D,
	}
	jwkDocumentJson, err := json.Marshal(jwkDocument)
	assert.NoError(t, err)
	type args struct {
		jwk          []byte
		ecPrivateKey *ec.PrivateKey
		plaintext    []byte
		enc          HeaderEnc
		alg          HeaderAlg
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "ValidPlaintextToDocumentToPlaintextUsingECAndJWK",
			args: args{
				jwk:          jwkDocumentJson,
				ecPrivateKey: ecKeyPair,
				plaintext:    []byte("some-text"),
				enc:          A128GCM,
				alg:          ECDHES,
			},
			want:    []byte("some-text"),
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			document, err := NewDocumentWithEPK(tt.args.plaintext, tt.args.enc, tt.args.alg, tt.args.jwk, EncryptUsingECWithJWK)
			if !tt.wantErr(t, err, fmt.Sprintf("NewDefaultDocument(%v, %v, %v, %v)", tt.args.plaintext, tt.args.enc, tt.args.alg, tt.args.jwk)) {
				return
			}
			plaintext, err := ToPlaintext(tt.args.ecPrivateKey, document, DecryptUsingEC)
			if !tt.wantErr(t, err, fmt.Sprintf("ToPlaintext(%v, %v)", tt.args.ecPrivateKey, document)) {
				return
			}
			assert.Equalf(t, tt.want, plaintext, "NewDocument(%v, %v, %v, %v), ToPlaintext(%v, %v)", tt.args.plaintext, tt.args.enc, tt.args.alg, tt.args.jwk, tt.args.ecPrivateKey, document)
		})
	}
}

func TestNewDefaultDocument(t *testing.T) {
	sessionAesKey, err := aes.GenerateKey(aes.AES256)
	assert.NoError(t, err)
	type args struct {
		aesKey    []byte
		plaintext []byte
	}
	tests := []struct {
		name       string
		args       args
		want       []byte
		wantHeader string
		wantErr    assert.ErrorAssertionFunc
	}{
		{
			name: "ValidNewDefaultDocument",
			args: args{
				aesKey:    sessionAesKey,
				plaintext: []byte("some-text"),
			},
			want:       []byte("some-text"),
			wantHeader: "eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2R0NNIn0",
			wantErr:    assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewDefaultDocument(tt.args.aesKey, tt.args.plaintext)
			if !tt.wantErr(t, err, fmt.Sprintf("NewDefaultDocument(%v, %v)", tt.args.aesKey, tt.args.plaintext)) {
				return
			}

			expectedNumberOfDocumentParts := 5
			gotDocumentParts := strings.Split(got, ".")
			if len(gotDocumentParts) != expectedNumberOfDocumentParts {
				t.Errorf("Got %v Document parts, want %v", len(gotDocumentParts), expectedNumberOfDocumentParts)
			}

			gotHeader := gotDocumentParts[0]
			assert.Equalf(t, tt.wantHeader, gotHeader, "NewDefaultDocument(%v, %v), Header", tt.args.aesKey, tt.args.plaintext)

			gotEncryptedKey := gotDocumentParts[1]
			assert.Equalf(t, "", gotEncryptedKey, "NewDefaultDocument(%v, %v), Encrypted Key", tt.args.aesKey, tt.args.plaintext)

			gotInitializationVector, err := base64.Base64String(gotDocumentParts[2]).ToBytes()
			assert.NoError(t, err)
			gotCiphertext, err := base64.Base64String(gotDocumentParts[3]).ToBytes()
			assert.NoError(t, err)
			gotAuthenticationTag, err := base64.Base64String(gotDocumentParts[4]).ToBytes()
			assert.NoError(t, err)
			plaintext, err := aes.Decrypt(sessionAesKey, gotInitializationVector, gotCiphertext, gotAuthenticationTag)
			assert.NoError(t, err)
			assert.Equalf(t, tt.want, plaintext, "NewDefaultDocument(%v, %v), got %s, want %s", tt.args.aesKey, tt.args.plaintext, plaintext, tt.want)
		})
	}
}

func TestNewDocument1(t *testing.T) {
	sessionAesKey, err := aes.GenerateKey(aes.AES256)
	assert.NoError(t, err)
	type args struct {
		aesKey    []byte
		plaintext []byte
		enc       HeaderEnc
		alg       HeaderAlg
	}
	tests := []struct {
		name       string
		args       args
		want       []byte
		wantHeader string
		wantErr    assert.ErrorAssertionFunc
	}{
		{
			name: "ValidNewDocument",
			args: args{
				aesKey:    sessionAesKey,
				plaintext: []byte("some-text"),
				enc:       A128GCM,
				alg:       DIR,
			},
			want:       []byte("some-text"),
			wantHeader: "eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4R0NNIn0",
			wantErr:    assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewDocument(tt.args.aesKey, tt.args.plaintext, tt.args.enc, tt.args.alg, EncryptUsingAES)
			if !tt.wantErr(t, err, fmt.Sprintf("NewDocument(%v, %v)", tt.args.aesKey, tt.args.plaintext)) {
				return
			}

			expectedNumberOfDocumentParts := 5
			gotDocumentParts := strings.Split(got, ".")
			if len(gotDocumentParts) != expectedNumberOfDocumentParts {
				t.Errorf("Got %v Document parts, wantHeader %v", len(gotDocumentParts), expectedNumberOfDocumentParts)
			}

			gotHeader := gotDocumentParts[0]
			assert.Equalf(t, tt.wantHeader, gotHeader, "NewDocument(%v, %v), Header", tt.args.aesKey, tt.args.plaintext)

			gotEncryptedKey := gotDocumentParts[1]
			assert.Equalf(t, "", gotEncryptedKey, "NewDocument(%v, %v), Encrypted Key", tt.args.aesKey, tt.args.plaintext)

			gotInitializationVector, err := base64.Base64String(gotDocumentParts[2]).ToBytes()
			assert.NoError(t, err)
			gotCiphertext, err := base64.Base64String(gotDocumentParts[3]).ToBytes()
			assert.NoError(t, err)
			gotAuthenticationTag, err := base64.Base64String(gotDocumentParts[4]).ToBytes()
			assert.NoError(t, err)
			plaintext, err := aes.Decrypt(sessionAesKey, gotInitializationVector, gotCiphertext, gotAuthenticationTag)
			assert.NoError(t, err)
			assert.Equalf(t, tt.want, plaintext, "NewDocument(%v, %v), got %s, want %s", tt.args.aesKey, tt.args.plaintext, plaintext, tt.want)
		})
	}
}

func TestToPlaintext(t *testing.T) {
	sessionAesKey, err := aes.GenerateKey(aes.AES256)
	assert.NoError(t, err)
	encryptedKey, initializationVector, ciphertext, authenticationTag, err := bodyPartsFromString(sessionAesKey, []byte("some-text"), EncryptUsingAES)
	assert.NoError(t, err)
	encodedInitializationVector := initializationVector.asEncodedString()
	encodedEncryptedCiphertext := ciphertext.asEncodedString()
	encodedAuthenticationTag := authenticationTag.asEncodedString()
	encodedEncryptedKey := encryptedKey.asEncodedString()
	type args struct {
		aesKey   []byte
		document string
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "",
			args: args{
				aesKey:   sessionAesKey,
				document: fmt.Sprintf("eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2R0NNIn0.%s.%s.%s.%s", encodedEncryptedKey, encodedInitializationVector, encodedEncryptedCiphertext, encodedAuthenticationTag),
			},
			want:    []byte("some-text"),
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ToPlaintext(tt.args.aesKey, tt.args.document, DecryptUsingAES)
			if !tt.wantErr(t, err, fmt.Sprintf("ToPlaintext(%v, %v)", tt.args.aesKey, tt.args.document)) {
				return
			}
			assert.Equalf(t, tt.want, got, "ToPlaintext(%v, %v)", tt.args.aesKey, tt.args.document)
		})
	}
}

func TestNewDocument(t *testing.T) {
	sessionAesKey, err := aes.GenerateKey(aes.AES128)
	assert.NoError(t, err)
	encryptedKey, initializationVector, ciphertext, authenticationTag, err := bodyPartsFromString(sessionAesKey, []byte("test"), EncryptUsingAES)
	assert.NoError(t, err)
	type args struct {
		header               *Header
		encryptedKey         EncryptedKey
		initializationVector InitializationVector
		ciphertext           Ciphertext
		authenticationTag    AuthenticationTag
	}
	tests := []struct {
		name string
		args args
		want *Document
	}{
		{
			name: "ValidNewDocument",
			args: args{
				header:               defaultHeader(),
				encryptedKey:         encryptedKey,
				initializationVector: initializationVector,
				ciphertext:           ciphertext,
				authenticationTag:    authenticationTag,
			},
			want: &Document{
				Header:               defaultHeader(),
				EncryptedKey:         encryptedKey,
				InitializationVector: initializationVector,
				Ciphertext:           ciphertext,
				AuthenticationTag:    authenticationTag,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := newDocument(tt.args.header, tt.args.encryptedKey, tt.args.initializationVector, tt.args.ciphertext, tt.args.authenticationTag); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("newDocument() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDocument_AsEncodedString(t *testing.T) {
	sessionAesKey, err := aes.GenerateKey(aes.AES128)
	assert.NoError(t, err)
	encryptedKey, initializationVector, ciphertext, authenticationTag, err := bodyPartsFromString(sessionAesKey, []byte("test"), EncryptUsingAES)
	assert.NoError(t, err)
	encodedEncryptedCiphertextString := ciphertext.asEncodedString()
	type fields struct {
		Header               *Header
		EncryptedKey         EncryptedKey
		InitializationVector InitializationVector
		Ciphertext           Ciphertext
		AuthenticationTag    AuthenticationTag
	}
	tests := []struct {
		name    string
		fields  fields
		want    string
		wantErr bool
	}{
		{
			name: "ValidDocumentAsEncodedString",
			fields: fields{
				Header:               defaultHeader(),
				EncryptedKey:         encryptedKey,
				InitializationVector: initializationVector,
				Ciphertext:           ciphertext,
				AuthenticationTag:    authenticationTag,
			},
			want:    fmt.Sprintf("eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2R0NNIn0.%s.%s.%s.%s", encryptedKey.asEncodedString(), initializationVector.asEncodedString(), encodedEncryptedCiphertextString, authenticationTag.asEncodedString()),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Document{
				Header:               tt.fields.Header,
				EncryptedKey:         tt.fields.EncryptedKey,
				InitializationVector: tt.fields.InitializationVector,
				Ciphertext:           tt.fields.Ciphertext,
				AuthenticationTag:    tt.fields.AuthenticationTag,
			}
			got, err := d.asEncodedString()
			if (err != nil) != tt.wantErr {
				t.Errorf("asEncodedString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("asEncodedString() got = %v, wantHeader %v", got, tt.want)
			}
		})
	}
}

func TestBodyPartsFromString(t *testing.T) {
	sessionAesKey, err := aes.GenerateKey(aes.AES128)
	assert.NoError(t, err)
	type args struct {
		aesKey    []byte
		plaintext string
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "ValidBodyPartsFromString",
			args: args{
				aesKey:    sessionAesKey,
				plaintext: "test",
			},
			want:    []byte("test"),
			wantErr: false,
		},
		{
			name: "InvalidBodyPartsFromString_WithShortAesKey",
			args: args{
				aesKey:    []byte("short"),
				plaintext: "test",
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, gotInitializationVector, gotCiphertext, gotAuthenticationTag, err := bodyPartsFromString(tt.args.aesKey, []byte(tt.args.plaintext), EncryptUsingAES)
			if (err != nil) != tt.wantErr {
				t.Errorf("bodyPartsFromString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err == nil {
				gotDecryptedText, err := aes.Decrypt(sessionAesKey, gotInitializationVector, gotCiphertext, gotAuthenticationTag)
				assert.NoError(t, err)
				if !slices.Equal(gotDecryptedText, tt.want) {
					t.Errorf("bodyPartsFromString() got = %v, wantHeader %v", gotCiphertext, tt.want)
				}
			}
		})
	}
}

func TestDefaultHeader(t *testing.T) {
	tests := []struct {
		name string
		want *Header
	}{
		{
			name: "ValidDefaultHeader",
			want: &Header{
				JOSEHeader: &jwt.JOSEHeader{
					Algorithm: "dir",
				},
				Encryption: A256GCM,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := defaultHeader(); !reflect.DeepEqual(got, tt.want) {
				gotJSON, _ := json.MarshalIndent(got, "", "  ")
				wantJSON, _ := json.MarshalIndent(tt.want, "", "  ")
				t.Errorf("defaultHeader() = %s, want %s", gotJSON, wantJSON)
			}
		})
	}
}

func TestNewHeader(t *testing.T) {
	type args struct {
		enc HeaderEnc
		alg HeaderAlg
	}
	tests := []struct {
		name string
		args args
		want *Header
	}{
		{
			name: "ValidHeader",
			args: args{
				enc: A256GCM,
				alg: "dir",
			},
			want: defaultHeader(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := newHeader(tt.args.enc, tt.args.alg); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("newHeader() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHeader_AsEncodedString(t *testing.T) {
	type fields struct {
		JOSEHeader *jwt.JOSEHeader
		Enc        HeaderEnc
	}
	tests := []struct {
		name    string
		fields  fields
		want    base64.Base64String
		wantErr bool
	}{
		{
			name: "ValidHeaderAsString",
			fields: fields{
				JOSEHeader: &jwt.JOSEHeader{
					Algorithm: "dir",
				},
				Enc: A128GCM,
			},
			want:    "eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4R0NNIn0",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &Header{
				JOSEHeader: tt.fields.JOSEHeader,
				Encryption: tt.fields.Enc,
			}
			got, err := h.asEncodedString()
			if (err != nil) != tt.wantErr {
				t.Errorf("asEncodedString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("asEncodedString() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHeader_AsString(t *testing.T) {
	type fields struct {
		JOSEHeader *jwt.JOSEHeader
		Enc        HeaderEnc
	}
	tests := []struct {
		name    string
		fields  fields
		want    string
		wantErr bool
	}{
		{
			name: "ValidHeaderAsString",
			fields: fields{
				JOSEHeader: &jwt.JOSEHeader{
					Algorithm: "dir",
				},
				Enc: A192GCM,
			},
			want:    "{\"alg\":\"dir\",\"enc\":\"A192GCM\"}",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &Header{
				JOSEHeader: tt.fields.JOSEHeader,
				Encryption: tt.fields.Enc,
			}
			got, err := h.asString()
			if (err != nil) != tt.wantErr {
				t.Errorf("asBytes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("asBytes() got = %v, wantHeader %v", got, tt.want)
			}
		})
	}
}

func TestEncryptedKey_AsEncodedString(t *testing.T) {
	tests := []struct {
		name string
		h    EncryptedKey
		want base64.Base64String
	}{
		{
			name: "ValidEncryptedKey_AsEncodedString",
			h:    EncryptedKey("test"),
			want: base64.Base64String("dGVzdA"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.h.asEncodedString()
			assert.Equalf(t, tt.want, got, "asEncodedString()")
		})
	}
}

func TestEncryptedKey_AsBytes(t *testing.T) {
	tests := []struct {
		name string
		h    EncryptedKey
		want []byte
	}{
		{
			name: "ValidEncryptedKey_AsBytes",
			h:    []byte("test"),
			want: []byte("test"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.h.asBytes()
			assert.Equalf(t, tt.want, got, "asBytes()")
		})
	}
}

func TestInitializationVectorFromEncodedString(t *testing.T) {
	type args struct {
		encodedInitializationVector base64.Base64String
	}
	tests := []struct {
		name    string
		args    args
		want    *InitializationVector
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "ValidInitializationVectorFromEncodedString",
			args: args{
				encodedInitializationVector: base64.Base64String("dGVzdA"),
			},
			want:    initializationVectorFromString([]byte("test")),
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := initializationVectorFromEncodedString(tt.args.encodedInitializationVector)
			if !tt.wantErr(t, err, fmt.Sprintf("initializationVectorFromEncodedString(%v)", tt.args.encodedInitializationVector)) {
				return
			}
			assert.Equalf(t, tt.want, got, "initializationVectorFromEncodedString(%v)", tt.args.encodedInitializationVector)
		})
	}
}

func TestInitializationVectorFromString(t *testing.T) {
	type args struct {
		initializationVectorAsString []byte
	}
	tests := []struct {
		name string
		args args
		want *InitializationVector
	}{
		{
			name: "ValidInitializationVectorFromString",
			args: args{
				initializationVectorAsString: []byte("test"),
			},
			want: initializationVectorFromString([]byte("test")),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, initializationVectorFromString(tt.args.initializationVectorAsString), "initializationVectorFromString(%v)", tt.args.initializationVectorAsString)
		})
	}
}

func TestInitializationVector_AsEncodedString(t *testing.T) {
	tests := []struct {
		name string
		h    InitializationVector
		want base64.Base64String
	}{
		{
			name: "ValidInitializationVector_AsEncodedString",
			h:    InitializationVector("test"),
			want: base64.Base64String("dGVzdA"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.h.asEncodedString()
			assert.Equalf(t, tt.want, got, "asEncodedString()")
		})
	}
}

func TestInitializationVector_AsBytes(t *testing.T) {
	tests := []struct {
		name string
		h    InitializationVector
		want []byte
	}{
		{
			name: "ValidInitializationVector_AsBytes",
			h:    []byte("test"),
			want: []byte("test"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.h.asBytes()
			assert.Equalf(t, tt.want, got, "asBytes()")
		})
	}
}

func TestCiphertext_AsEncodedString(t *testing.T) {
	sessionAesKey, err := aes.GenerateKey(aes.AES128)
	assert.NoError(t, err)
	_, _, ciphertext, _, err := bodyPartsFromString(sessionAesKey, []byte("test"), EncryptUsingAES)
	assert.NoError(t, err)
	tests := []struct {
		name string
		p    Ciphertext
		want base64.Base64String
	}{
		{
			name: "ValidCiphertextAsString",
			p:    ciphertext,
			want: base64.ParseFromBytes(ciphertext),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.asEncodedString(); got != tt.want {
				t.Errorf("asEncodedString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCiphertext_AsBytes(t *testing.T) {
	sessionAesKey, err := aes.GenerateKey(aes.AES128)
	assert.NoError(t, err)
	_, _, ciphertext, _, err := bodyPartsFromString(sessionAesKey, []byte("test"), EncryptUsingAES)
	assert.NoError(t, err)
	tests := []struct {
		name string
		p    Ciphertext
		want []byte
	}{
		{
			name: "ValidCiphertextAsBytes",
			p:    ciphertext,
			want: ciphertext,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.asBytes(); !slices.Equal(got, tt.want) {
				t.Errorf("asBytes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAuthenticationTagFromEncodedString(t *testing.T) {
	type args struct {
		encodedAuthenticationTag base64.Base64String
	}
	tests := []struct {
		name    string
		args    args
		want    *AuthenticationTag
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "ValidAuthenticationTagFromEncodedString",
			args: args{
				encodedAuthenticationTag: base64.Base64String("dGVzdA"),
			},
			want:    authenticationTagFromString([]byte("test")),
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := authenticationTagFromEncodedString(tt.args.encodedAuthenticationTag)
			if !tt.wantErr(t, err, fmt.Sprintf("authenticationTagFromEncodedString(%v)", tt.args.encodedAuthenticationTag)) {
				return
			}
			assert.Equalf(t, tt.want, got, "authenticationTagFromEncodedString(%v)", tt.args.encodedAuthenticationTag)
		})
	}
}

func TestAuthenticationTagFromString(t *testing.T) {
	type args struct {
		authenticationTagAsString []byte
	}
	tests := []struct {
		name string
		args args
		want *AuthenticationTag
	}{
		{
			name: "ValidAuthenticationTagFromString",
			args: args{
				authenticationTagAsString: []byte("test"),
			},
			want: authenticationTagFromString([]byte("test")),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, authenticationTagFromString(tt.args.authenticationTagAsString), "authenticationTagFromString(%v)", tt.args.authenticationTagAsString)
		})
	}
}

func TestAuthenticationTag_AsEncodedString(t *testing.T) {
	tests := []struct {
		name string
		h    AuthenticationTag
		want base64.Base64String
	}{
		{
			name: "ValidAuthenticationTag_AsEncodedString",
			h:    AuthenticationTag("test"),
			want: base64.Base64String("dGVzdA"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.h.asEncodedString()
			assert.Equalf(t, tt.want, got, "asEncodedString()")
		})
	}
}

func TestAuthenticationTag_AsBytes(t *testing.T) {
	tests := []struct {
		name string
		h    AuthenticationTag
		want []byte
	}{
		{
			name: "ValidAuthenticationTag_AsBytes",
			h:    []byte("test"),
			want: []byte("test"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.h.asBytes()
			assert.Equalf(t, tt.want, got, "asBytes()")
		})
	}
}
