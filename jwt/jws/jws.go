// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package jws

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/jwt"

	golangJwt "github.com/golang-jwt/jwt/v5"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ecdsa"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/encoding/base64"
)

// NewDefaultSignedDocument - takes a plaintext and private key and returns signed a Base64 encoded
// JWS Document with a default JWS Header.
func NewDefaultSignedDocument(plaintext []byte, privateKey *ec.PrivateKey) (string, error) {
	header := defaultHeader()
	return NewSignedDocument(plaintext, privateKey, HeaderType(header.Typ), HeaderAlg(header.Algorithm))
}

// NewSignedDocument - takes a plaintext, JWS type and JWS algorithm and returns a Base64 encoded JWS Document.
func NewSignedDocument(plaintext []byte, privateKey *ec.PrivateKey, headerType HeaderType, alg HeaderAlg) (string, error) {
	header := newHeader(headerType, alg)
	payload := Payload(plaintext)
	document, err := newSignedDocument(header, payload, privateKey)
	if err != nil {
		return "", err
	}
	documentAsString, err := document.asSignedEncodedString()
	if err != nil {
		return "", err
	}
	return documentAsString, nil
}

// NewDefaultDocument - takes a plaintext and returns a Base64 encoded
// JWS Document with a default JWS Header.
func NewDefaultDocument(plaintext []byte) (string, error) {
	header := defaultHeader()
	return NewDocument(plaintext, HeaderType(header.Typ), HeaderAlg(header.Algorithm))
}

// NewDocument - takes a plaintext, encryption and algorithm and returns a Base64 encoded and AES encrypted JWS Document.
func NewDocument(plaintext []byte, headerType HeaderType, alg HeaderAlg) (string, error) {
	header := newHeader(headerType, alg)
	payload := Payload(plaintext)
	document := newDocument(header, payload)
	documentAsString, err := document.asEncodedString()
	if err != nil {
		return "", err
	}
	return documentAsString, nil
}

// ToPlaintext - takes a Base64 encoded JWS Document and returns the payload as plaintext.
func ToPlaintext(jwsDocument string) ([]byte, error) {
	documentParts := strings.Split(jwsDocument, ".")
	if len(documentParts) != 2 &&
		len(documentParts) != 3 {
		return nil, fmt.Errorf("JWS Document string provided with %d parts, but JWS Document string is expected with 2 or 3 parts", len(documentParts))
	}

	payload, err := payloadFromEncodedString(base64.Base64String(documentParts[1]))
	if err != nil {
		return nil, err
	}

	return []byte(payload), nil
}

// Verify - verifies JWS document against public key.
func Verify(jwsDocument string, publicKey *ec.PublicKey) (bool, error) {
	documentParts := strings.Split(jwsDocument, ".")
	if len(documentParts) != 3 {
		return false, fmt.Errorf("JWS Document string provided with %d parts, but signed JWS Document string is expected with 3 parts", len(documentParts))
	}
	jwsHeader, err := base64.Base64String(documentParts[0]).ToBytes()
	if err != nil {
		return false, err
	}
	jwsPayload, err := base64.Base64String(documentParts[1]).ToBytes()
	if err != nil {
		return false, err
	}
	jwsSignature, err := base64.Base64String(documentParts[2]).ToBytes()
	if err != nil {
		return false, err
	}
	signingInput := createSigningInput(jwsHeader, jwsPayload)
	return ecdsa.Verify(publicKey, []byte(signingInput), jwsSignature), nil
}

// Document - the JWS specific document.
// Contains a JWS header, payload and optionally signature, seperated by a dot.
type Document struct {
	Header    *Header
	Payload   Payload
	Signature Signature
}

func newDocument(header *Header, payload Payload) *Document {
	return &Document{
		Header:  header,
		Payload: payload,
	}
}

func newSignedDocument(header *Header, payload Payload, privateKey *ec.PrivateKey) (*Document, error) {
	signature, err := newSignature(header, payload, privateKey)
	if err != nil {
		return nil, err
	}
	return &Document{
		Header:    header,
		Payload:   payload,
		Signature: signature,
	}, nil
}

// asEncodedString - The value returned is itself not Base64 encoded, but its parts are Base64 encoded, seperated by a "."
func (d *Document) asEncodedString() (string, error) {
	encodedHeaderAsJson, err := d.Header.asEncodedString()
	if err != nil {
		return "", err
	}

	encodedPayloadAsJson := d.Payload.asEncodedString()

	return fmt.Sprintf("%s.%s", encodedHeaderAsJson, encodedPayloadAsJson), nil
}

// asSignedEncodedString - The value returned is itself not Base64 encoded, but its parts are Base64 encoded, seperated by a "."
func (d *Document) asSignedEncodedString() (string, error) {
	encodedHeaderAsJson, err := d.Header.asEncodedString()
	if err != nil {
		return "", err
	}

	encodedPayloadAsJson := d.Payload.asEncodedString()

	encodedSignature := d.Signature.asEncodedString()

	return fmt.Sprintf("%s.%s.%s", encodedHeaderAsJson, encodedPayloadAsJson, encodedSignature), nil
}

// Header - the JWS specific header.
type Header struct {
	*jwt.JOSEHeader
	Typ string `json:"typ"`
}

type HeaderType string
type HeaderAlg string

const (
	JWT HeaderType = "jwt"
)

func defaultHeader() *Header {
	return &Header{
		JOSEHeader: &jwt.JOSEHeader{
			Algorithm: golangJwt.SigningMethodES256.Name,
		},
		Typ: string(JWT),
	}
}

func newHeader(typ HeaderType, alg HeaderAlg) *Header {
	return &Header{
		JOSEHeader: &jwt.JOSEHeader{
			Algorithm: string(alg),
		},
		Typ: string(typ),
	}
}

func (h *Header) asEncodedString() (base64.Base64String, error) {
	headerAsString, err := h.asString()
	if err != nil {
		return "", err
	}
	return base64.ParseFromBytes([]byte(headerAsString)), nil
}

func (h *Header) asString() (string, error) {
	headerAsJson, err := json.Marshal(h)
	if err != nil {
		return "", fmt.Errorf("error occurred while marshalling JWS Header. Error: %v", err)
	}
	return string(headerAsJson), nil
}

// Payload - the JWS specific payload.
type Payload string

func payloadFromEncodedString(encodedPayload base64.Base64String) (Payload, error) {
	payload, err := encodedPayload.ToBytes()
	if err != nil {
		return "", fmt.Errorf("error occurred while Base64 decoding JWS Payload. Error: %v", err)
	}

	return Payload(payload), nil
}

func (p *Payload) asEncodedString() base64.Base64String {
	return base64.ParseFromBytes([]byte(*p))
}

/**
Signature, the JWS specific signature.
The signature is Base64 decoded internally and exposed as Base64 encoded.
*/

type Signature string

func newSignature(header *Header, payload Payload, privateKey *ec.PrivateKey) (Signature, error) {
	headerAsJson, err := header.asString()
	if err != nil {
		return "", err
	}
	payloadAsJson := string(payload)
	signingInput := createSigningInput([]byte(headerAsJson), []byte(payloadAsJson))
	signature, err := ecdsa.Sign(privateKey, []byte(signingInput))
	if err != nil {
		return "", fmt.Errorf("error occurred while creating JWS signature. Error: %v", err)
	}
	return Signature(signature), nil
}

func (s *Signature) asEncodedString() base64.Base64String {
	return base64.ParseFromBytes([]byte(*s))
}

func createSigningInput(headerAsJson []byte, payloadAsJson []byte) string {
	return fmt.Sprintf("%s.%s", headerAsJson, payloadAsJson)
}
