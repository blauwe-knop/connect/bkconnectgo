// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package jwt

/**
JOSEHeader, the base of all JWT headers.
*/

type JOSEHeader struct {
	Algorithm string `json:"alg"`
	Kid       string `json:"kid,omitempty"`
}
