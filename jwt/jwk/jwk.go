// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package jwk

import (
	"math/big"
)

// Document - the JWK specific document.
type Document struct {
	KeyType DocumentKeyType `json:"kty"`
	Curve   string          `json:"crv"`
	X       *big.Int        `json:"x"`
	Y       *big.Int        `json:"y"`
	D       *big.Int        `json:"d"`
}

type DocumentKeyType string

const (
	EC DocumentKeyType = "EC"
)
